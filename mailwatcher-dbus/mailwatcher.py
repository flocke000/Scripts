#! /usr/bin/python2
#
# mailwatcher.py - An IMAP Mailchecker for the Awesome WM
# Copyright (c) 2012-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
##########
#
# To use this Script add your mail account(s) to the mailwatcher.ini file and
# edit the marked line in this file to point to your configuration file.
# You can run the script in the background by adding 'python2 /path/to/mailwatcher.py &'
# to your .xinitrc.
# This will check for new mail every 5 min and publish it via DBus as table.
#
# To display this information in the awesome wm add this simple textwidget to your rc.lua:
#
#   local mailwidget = widget({ type = "textbox", name = "mailwidget" })
#   dbus.request_name("session", "com.flocke.MailWatcher")
#   dbus.add_match("session", "interface='com.flocke.MailWatcher',member='Changed'")
#   dbus.add_signal("com.flocke.MailWatcher", function(...)
#       local data = {...}
#		local string = "["
#		for name, value in pairs(data[2]) do
#			string = string .. name .. ": " .. value .. "] ["
#		end
#		mailwidget.text = string.sub(string, 1, -3)
#   end)
#
# Of course you can use the DBus signal from this script in every WM or application
# that can listen to DBus other than the awesome wm.
#


import time
import re
import threading
import dbus
import dbus.service
import dbus.mainloop.glib
import ConfigParser
from imaplib import *

class MailWatcher(dbus.service.Object):
	def __init__(self, name, session):
		dbus.service.Object.__init__(self, name, session)
	
	@dbus.service.signal('com.flocke.MailWatcher', signature='a{ss}')
	def Changed(self, data):
		pass	

class MailCheck(threading.Thread):
	def __init__(self, name, url, user, passwd, mailbox, ssl):
		threading.Thread.__init__(self)
		self.name = name
		self.user = user
		self.passwd = passwd
		self.url = url
		self.mailbox = mailbox
		self.ssl = ssl
		self.result = "N/A"

	def run(self):
		if self.ssl == "True":
			server = IMAP4_SSL(self.url)
		else:
			server = IMAP4(self.url)
		server.login(self.user, self.passwd)
		self.result = re.search("UNSEEN (\d+)", server.status(self.mailbox, "(UNSEEN)")[1][0]).group(1)

dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

session_bus = dbus.SessionBus()

name = dbus.service.BusName('com.flocke.MailWatcher', session_bus)
watcher = MailWatcher(session_bus, '/com/flocke/MailWatcher')

parser = ConfigParser.SafeConfigParser()

#### Edit to point to your configuration file
parser.read('/path/to/mailwatcher.ini')

while True:
	data = {}
	threads_running = []
	for section in parser.sections():
		account = {}
		for name, value in parser.items(section):
			account[name] = value
		thread_current = MailCheck(section, account['server'], account['user'], account['pass'], account['mailbox'], account['ssl'])
		threads_running.append(thread_current)
		thread_current.start()
	for th in threads_running:
		th.join()
		data[th.name] = th.result
	watcher.Changed(data)
    #### Edit if you want a different intervall (in seconds)
	time.sleep(300)
