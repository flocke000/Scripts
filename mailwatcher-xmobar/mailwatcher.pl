#! /usr/bin/perl
#
# mailwatcher.pl - An IMAP mailchecker for xmobar with optional desktop notifications.
# Copyright (c) 2013-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#######################################################################
#
# To use this script copy the sample config file to ~/.config/mailwatcher/config.ini
# and add the script to your .xmobarrc with the Com plugin.
# Choose a sane timeout value like 300 sec!
# Example:
#  Run Com "/path/to/mailwatcher.pl" [] "mail" 3000
#

use strict;
use warnings;

use threads;

use Config::INI::Reader;
use Net::IMAP::Client;
use Desktop::Notify;

#########################################################################
# Function definitions
#########################################################################

sub get_mail($$);
sub print_formated($$);
sub send_notification($$$);

#########################################################################
# Load configuration
#########################################################################

# Set default values
my $config_file = "config.ini";
my $config_dir = "$ENV{HOME}/.config/mailwatcher";

my $general_config = {
  notify => 1,
  notify_timeout => 10,
  format => "%_total%"
};

# Parse config file
die "ERROR: No ${config_file} found under ${config_dir}" if (! -f "${config_dir}/${config_file}");

my $config_hash = Config::INI::Reader -> read_file("${config_dir}/${config_file}");

# Overwrite default config with values from the file
if ($config_hash -> {"general"}) {
  for my $key (keys($general_config)) {
    if ($config_hash -> {"general"} -> {$key}) {
      $general_config -> {$key} = $config_hash -> {"general"} -> {$key};
    }
  }
  delete $config_hash -> {"general"};
}

#########################################################################
# Main app
#########################################################################

# Start a thread for each mailbox
my $threads = {};
for my $account (keys($config_hash)) {
  $threads -> {$account} = threads -> create(\&get_mail, $account, $config_hash -> {$account});
}

# Wait for the threads to return the number of unseen mails and add them
my $output_keys = {};
$output_keys -> {"_total"} = 0;
for my $thread (keys($threads)) {
  my $this_count = $threads -> {$thread} -> join();
  $output_keys -> {$thread} = $this_count;
  $output_keys -> {"_total"} += $this_count;
}

# Format the output string
print_formated($general_config -> {"format"}, $output_keys);
  
# Exit
exit 0;

#########################################################################
# Functions
#########################################################################

# Apply the format string to the new mail counts
sub print_formated($$) {
  my ($string, $hash) = @_;

  while (my ($key, $value) = each(%$hash)) {
    $string =~ s/%$key%/$value/g;
  }

  print $string;
}

# Send a desktop notification
sub send_notification($$$) {
  my ($title, $body, $timeout) = @_;
  
  my $notifier = Desktop::Notify -> new();

  my $notification = $notifier -> create(
    summary => $title,
    body => $body,
    timeout => $timeout
  );

  $notification -> show();
}

# Fetch the mails for one mailbox
sub get_mail($$) {
  my ($name, $account) = @_;

  # Generate IMAP config for the given account
  my $imap_config = {};
  my $missing = 0;

  # Load necessary values
  for my $key ("server", "user", "pass") {
    if ($account -> {$key}) {
      $imap_config -> {$key} = $account -> {$key};
    } else {
      warn "WARN: Missing option '${key}' for account ${name}.";
      $missing = 1;
    }
  }

  # Load optional values
  for my $key ("ssl", "port") {
    if ($account -> {$key}) {
      $imap_config -> {$key} = $account -> {$key};
    }
  }

  # Abort if a necessary value is missing
  return if ($missing);

  # Load mailbox to check
  my $mailbox = "INBOX";
  if ($account -> {"mailbox"}) {
    $mailbox = $account -> {"mailbox"};
  }

  # Connect to the IMAP server
  my $client = Net::IMAP::Client -> new(%$imap_config);
  $client -> login or warn "WARN: IMAP login failed for account ${name}";

  # Get new messages in the selected Mailbox
  $client -> select($mailbox);
  my $unseen = $client -> search('UNSEEN');
  my $num = scalar(@$unseen);

  # Create a notification for each new message
  if ($general_config -> {"notify"}) {
    my $new = $client -> search('NEW');
    my $sums = $client -> get_summaries($new);
    
    foreach (@$sums) {
      send_notification("New mail in mailbox '${name}'", "From: " . join(", ", @{$_ -> from}) . "\n" . "Subject: " . $_ -> subject, ($general_config -> {"notify_timeout"} * 1000));
    }
  }

  # IMAP Logout
  $client -> logout;

  return($num);
}
