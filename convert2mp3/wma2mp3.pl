#! /usr/bin/perl -w
# Copyright (c) 2010-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

use Audio::WMA;
use MP3::Tag;

sub help;
sub file;
sub all;
sub version;
sub unknown;
sub convert;

if ($ARGV[0]) {
	if($ARGV[0] eq "--help" or $ARGV[0] eq "-h") { help };
	if($ARGV[0] eq "--file" or $ARGV[0] eq "-f") { file };
	if($ARGV[0] eq "--all" or $ARGV[0] eq "-a") { all }  else { unknown };
} else { help };

sub help {
print "wma2mp3\n";
print "Usage: wma2mp3 [-a | -f FILE]\n\n";
print "Options:\n";
print "  -h,  --help				show this help\n";
print "  -a,  --all				convert all WMAs in the current dir\n";
print "  -f,  --file				only converts a single file\n";
exit 0;
}

sub unknown {
print "Unknown option!\n";
print "Use 'wma2mp3 -h' for help\n";
exit 0;
}

sub file {
if ($ARGV[1]) {
	convert($ARGV[1]);
	exit 0;
} else {
	print "Please give a file to convert!\n";
	exit 0;
}}

sub all {
print "Converting all files\n";
my @wmas = glob "*.wma";
foreach (@wmas) {
	my $wmafile = $_;
	convert($wmafile);
}
print "All done\n";
exit 0;
}

sub convert {
my ($filename) = @_; 
my $BEFEHL = "ffmpeg -i TEMP.wma -ab 128k TEMP.mp3 &> /dev/zero";
my $wma = Audio::WMA->new($filename);
my $tags = $wma->tags();
rename $filename , "TEMP.wma";
print "[$filename]: Converting\n";
qx($BEFEHL);
print "[$filename]: Reading tags\n";
my $artist = $tags->{"AUTHOR"};
my $track = $tags->{"TRACKNUMBER"};
my $album = $tags->{"ALBUMTITLE"};
my $title = $tags->{"TITLE"};
my $genre = $tags->{"GENRE"};
my $year = $tags->{"YEAR"};
$filename =~ s/wma/mp3/;
print "[$filename]: Writing tags\n";
MP3::Tag -> config('write_v24' => TRUE);
my $mp3 = MP3::Tag -> new("TEMP.mp3");
$mp3 -> new_tag("ID3v2");
if ($artist) { $mp3 -> {ID3v2} -> add_frame("TPE1", $artist) };
if ($title) { $mp3 -> {ID3v2} -> add_frame("TIT2", $title) };
if ($album) { $mp3 -> {ID3v2} -> add_frame("TALB", $album) };
if ($year) { $mp3 -> {ID3v2} -> add_frame("TYER", $year) };
if ($genre) { $mp3 -> {ID3v2} -> add_frame("TCON", $genre) };
if ($track) { $mp3 -> {ID3v2} -> add_frame("TRCK", $track) };
$mp3 -> {ID3v2} -> add_frame("COMM", "ENG", "Short text", "converted using wma2mp3 v1.3");
$mp3 -> {ID3v2} -> write_tag;
$mp3 -> close;
rename "TEMP.mp3" , $filename;
unlink "TEMP.wma";
print "[$filename]: Done\n\n";
}
