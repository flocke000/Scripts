#! /usr/bin/perl -w
# Copyright (c) 2010-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

use Ogg::Vorbis::Header::PurePerl;
use MP3::Tag;

sub help;
sub file;
sub all;
sub version;
sub unknown;
sub convert;

if ($ARGV[0]) {
	if($ARGV[0] eq "--help" or $ARGV[0] eq "-h") { help };
	if($ARGV[0] eq "--file" or $ARGV[0] eq "-f") { file };
	if($ARGV[0] eq "--all" or $ARGV[0] eq "-a") { all } else { unknown };
} else { help };

sub help {
print "ogg2mp3 v1.0\n";
print "Usage: ogg2mp3 [-a | -f FILE]\n\n";
print "Options:\n";
print "  -h,  --help				show this help\n";
print "  -a,  --all				convert all OGGs in the current dir\n";
print "  -f,  --file				only converts a single file\n";
exit 0;
}

sub unknown {
print "Unknown option!\n";
print "Use 'ogg2mp3 -h' for help\n";
exit 0;
}

sub file {
if ($ARGV[1]) {
	convert($ARGV[1]);
	exit 0;
} else {
	print "Please give a file to convert!\n";
	exit 0;
}}

sub all {
print "Converting all files\n";
my @oggs = glob "*.ogg";
foreach (@oggs) {
	my $oggfile = $_;
	convert($oggfile);
}
print "All done\n";
exit 0;
}

sub convert {
my ($filename) = @_; 
my $BEFEHL = "ffmpeg -i TEMP.ogg -ab 128k TEMP.mp3 &> /dev/zero";
my $ogg = Ogg::Vorbis::Header::PurePerl->new($filename);
print "[$filename]: Reading tags\n";
my @art = $ogg -> comment("artist");
my @trk = $ogg -> comment("tracknumber");
my @alb = $ogg -> comment("album");
my @tit = $ogg -> comment("title");
my @gen = $ogg -> comment("genre");
my $artist = join(',', @art);
my $track = join(',', @trk);
my $album = join(',', @alb);
my $title = join(',', @tit);
my $genre = join(',', @gen);
rename $filename , "TEMP.ogg";
print "[$filename]: Converting\n";
qx($BEFEHL);
$filename =~ s/ogg/mp3/;
print "[$filename]: Writing tags\n";
MP3::Tag -> config('write_v24' => TRUE);
my $mp3 = MP3::Tag -> new("TEMP.mp3");
$mp3 -> new_tag("ID3v2");
if ($artist) { $mp3 -> {ID3v2} -> add_frame("TPE1", $artist) };
if ($title) { $mp3 -> {ID3v2} -> add_frame("TIT2", $title) };
if ($album) { $mp3 -> {ID3v2} -> add_frame("TALB", $album) };
if ($genre) { $mp3 -> {ID3v2} -> add_frame("TCON", $genre) };
if ($track) { $mp3 -> {ID3v2} -> add_frame("TRCK", $track) };
$mp3 -> {ID3v2} -> add_frame("COMM", "ENG", "Short text", "converted using ogg2mp3 v1.0");
$mp3 -> {ID3v2} -> write_tag;
$mp3 -> close;
rename "TEMP.mp3" , $filename;
unlink "TEMP.ogg";
print "[$filename]: Done\n\n";
}
