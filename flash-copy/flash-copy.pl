#! /usr/bin/perl
# Copyright (c) 2010-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

use strict;
use warnings;

use File::Copy;
use File::Which;
use Getopt::Long;

my $flash = get_flash_files();
my $count = scalar(keys(%$flash));

my $num = 0;
my $list = 0;
my $help = 0;

my $res = GetOptions (
  "number|n=i" => \$num,
  "list|l" => \$list,
  "help|h" => \$help
);

if ($help) {
  print_help();
} elsif ($list) {
  if ($count == 0) {
    print "No flash videos found.\n";
  } else {
    list_files($flash, $count);
  }
} else {
  if ($ARGV[0]) {
    if ($count == 1) {
      copy_flash($flash, 0, $ARGV[0]);
    } elsif ($count > 1) {
      if ($num != 0) {
        copy_flash($flash, $num - 1, $ARGV[0]);
      } else {
        print "Multiple files detected.\nUse -l to list them and -n to select one.\n";
      }
    } else {
      print "No flash videos found.\n";
    }
  } else {
    print_help();
  }
}

exit 0;

##########################################################
##### Functions

sub copy_flash {
  my ($hash, $index, $target) = @_;

  my @keys = sort(keys(%$hash));

  if (-d $target) {
    $target = $target . "/" . $keys[$index] . ".flv";
  }
  
  copy($hash->{$keys[$index]}->{file}, $target) or
    die "Could not copy file '" . $hash->{$keys[$index]}->{file} . "' to '" . $target . "'.\n";
  print "Copied '" . $hash->{$keys[$index]}->{file} . "' to '" . $target . "'.\n";
}

sub print_help {
  print "flash-copy [-n NUM] [-l] [TARGET]\n\n";
  print "Without options this script copies the currently open flash video to TARGET.\n\n";
  print "Options:\n";
  print "  -l,--list\t\tList all found flash videos.\n";
  print "  -n,--number=NUM\tSpecify the video to copy.\n\n";
}

sub list_files {
  my ($lref, $count) = @_;

  my @list = sort(keys(%$lref));

  print "Video files:\n";
  for (my $i = 1; $i <= $count; $i++) {
    my $name = $list[$i-1];
    print "  $i) $name  ($lref->{$name}->{file}, $lref->{$name}->{size}";
    if ($lref->{$name}->{duration}) { print ", " . $lref->{$name}->{duration} };
    print ")\n";
  }
}

sub convert_size {
  my ($size) = @_;

  my $suffix = "KB";
  $size /= 1024;

  if ($size > 1024) {
    $size /= 1024;
    $suffix = "MB";
  }

  return sprintf("%.0f", $size) . " $suffix";
}

sub convert_time {
  my ($time) = @_;

  my $sec = sprintf("%02u", $time%60);
  my $min = sprintf("%02u", ($time/60)%60);
  my $hour = sprintf("%02u", ($time/(60*60))%24);

  my $string = "$hour:$min:$sec";

  return $string;
}

sub get_video_duration {
  my ($file) = @_;

  if (my $ffprobe = which("ffprobe")) {
    my $data = `$ffprobe -show_format $file 2> /dev/null`;
    $data =~ m/\nduration=(.+?)\n/;
    return convert_time($1);
  }
}

sub get_flash_files {
  my @files = glob("/proc/*/fd/*");

  my $targets = {};

  for my $file (@files) {
    if (-l $file) {
      my $target = readlink($file);
      if ($target =~ m/(Flash.+?)\s/) {
        $targets->{$1}->{file} = $file;
        $targets->{$1}->{size} = convert_size(-s $file);
        $targets->{$1}->{duration} = get_video_duration($file);
      }
    }
  }

  return($targets);
}

