#! /usr/bin/perl
#
# url-extract.pl - Extract and open URLs from files or STDIN
# Copyright (c) 2014-2015 Jakob Nixdorf <flocke@shadowice.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#########################################################################################
#
# To use this script you need following additional perl modules:
#  * libwww-perl (to fetch the titles for the extracted URLs)
#  * Curses::UI (to display the list)
#
# Run the script with a file as first argument or pipe your
# data into it via STDIN.
#
# Controlls in the list-view:
#  ENTER  -   Open the selected URL and exit
#  o      -   Open the selected URL without exiting
#  f      -   Fetch the title for the selected URL
#  F      -   Fetch the titles for all URLs (WARNING: Experimental, see $AUTO_FETCH_TITLES)
#  q      -   Exit
#
# Example binding for tmux to extraxt all currently visible URLs:
#  bind-key u capture-pane \; new-window -n "urlview" '$SHELL -c "tmux show-buffer | perl /path/to/url-extract.pl && tmux delete-buffer"'
#

#########################################################################################
# External modules

use strict;
use warnings;
use threads;
use Thread::Queue;
use Curses;
use Curses::UI;
use LWP::UserAgent;

#########################################################################################
# Config

# This is the regular expression used to match the URLs.
my $URL_REGEX = '\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))';
# Use this command to open the selected URL.
my $OPEN_COMMAND = 'firefox -new-tab "%s"';
# Use a background task to fetch the titles of all URLs and update the list.
# WARNING: This feature is still experimental and can be unstable!
my $AUTO_FETCH_TITLES = 0;

#########################################################################################
# Main application

my @lines;
if ( ! -t STDIN ) {
  # Read the data from STDIN
  @lines = <STDIN>;
  close ( STDIN );
  # Reacquire STDIN to get Curses::UI working
  open ( STDIN, "< /dev/tty" );
} else {
  if ( $ARGV[0] && -f $ARGV[0] ) {
    # Read the data from file
    open ( FILE, "< " . $ARGV[0] );
    @lines = <FILE>;
    close ( FILE );
  }
}

# Parse the data and get all URLs
my @urls = extract_urls ( join ( "\n", @lines ) );

if ( @urls ) {
  # Create the Curses::UI main object
  my $cui = new Curses::UI (
    -clean_on_exit => 1
  );

  # Add a window to display the list
  my $screen = $cui -> add (
    'screen', 'Window',
    -title => 'UrlExtract',
    -border => 0
  );

  # Add the list and fill it with all URLs
  my $list = $screen -> add (
    'list', 'Listbox',
    -values => \@urls
  );

  # Setup the queue to update the list
  my $queue = Thread::Queue -> new();

  # Bind the queue to the listbox
  $list -> {_queue} = $queue;

  # If enabled start the thread to fetch all titles
  if ( $AUTO_FETCH_TITLES ) { threads -> create ( \&fetch_urls_thread, $list ) };

  # Clear some unneeded keybindings
  $list -> clear_binding ( 'loose-focus' );
  $list -> clear_binding ( 'option-select' );
  $list -> clear_binding ( 'option-check' );
  $list -> clear_binding ( 'option-uncheck' );
  $list -> clear_binding ( 'search-forward' );
  $list -> clear_binding ( 'search-backward' );

  # Set keybindings for the list object
  $list -> set_binding ( sub { open_url ( $_[0] -> get_active_value() ); exit_main ( $cui ) }, KEY_ENTER() );
  $list -> set_binding ( sub { open_url ( $_[0] -> get_active_value() ) }, "o" );
  $list -> set_binding ( \&fetch_title, "f" );
  $list -> set_binding ( sub { threads -> create ( \&fetch_urls_thread, shift() ) }, "F" );

  # Set keybindings for the master object
  $cui -> set_binding ( \&exit_main, "q" );

  # Add the timer to read the queue and update the list
  $cui -> set_timer ( 'read_queue', sub {
    if ( defined ( my $item = $queue -> dequeue_nb() ) ) {
      $list -> add_labels ( $item );
    }
  });

  # Focus the list and start the application main-loop
  $list -> focus();
  $cui -> mainloop();
}

exit 0;

#########################################################################################
# Functions

# Fetch the title for one URL and update the queue
sub fetch_title {
  # Get the currently marked item from the list
  my $url = $_[0] -> get_active_value();

  # Start the LWP::UserAgent and fetch the URL
  my $ua = LWP::UserAgent -> new();
  my $resp = $ua -> get ( $url );

  # Update the queue if the request is successful
  if ( $resp -> is_success ) {
    $_[0] -> {_queue} -> enqueue ( { $url => $url . " (" . $resp -> title . ")" } );
  }
}

# Thread to fetch the titles of all URLs
sub fetch_urls_thread {
  # Kill the thread on SIGSTOP
  $SIG{'STOP'} = sub { threads -> exit() };

  # Start the LWP::UserAgent
  my $ua = LWP::UserAgent -> new();

  # Loop over all URLs and fetch the titles
  for ( my $i = 0; $i < scalar ( @urls ); $i++ ) {
    my $resp = $ua -> get ( $urls[$i] );
    # Update the queue if the request is successful
    if ( $resp -> is_success ) {
      $_[0] -> {_queue} -> enqueue ( { $urls[$i] => $urls[$i] . " (" . $resp -> title . ")" } );
    }
  }

  # Kill the thread
  threads -> exit();
}

# Cleanup and exit
sub exit_main {
  # Join all started threads
  foreach my $thread ( threads -> list() ) {
    # Kill the thread if it's still running
    if ( $thread -> is_running() ) {
      $thread -> kill ( 'STOP' );
    }
    $thread -> join();
  }

  # End the application main-loop
  $_[0] -> mainloopExit();
}

# Extract all URLs from a data string
sub extract_urls {
  my ( $data ) = @_;
 
  my @urls;
  if ( $data ) {
    my $idx = 1;
    # Parse the data with a regular expression
    while ( $data =~ m/$URL_REGEX/g ) {
      my $url = $1;
      $url =~ s/\n$//;
      # Push all found URLs into the list
      push ( @urls, $url );
    }
  }
  
  # Return the list of all URLs
  return ( @urls );
}

# Open a URL with a user-defined command
sub open_url {
  my ($url) = @_;

  # Load the command and insert the URL
  my $command = $OPEN_COMMAND;
  $command =~ s/%s/$url/g;

  # Silently run the command
  system ( $command . " > /dev/null 2>&1" );
}

