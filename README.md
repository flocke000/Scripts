# flocke's scripts

This repository contains some of the scripts I wrote to help me with my daily tasks.
If there are errors with some older scripts you can send a mail to flocke@shadowice.org and I will try to fix them if I have time.
The scripts should be mostly under the MIT license unless stated otherwise in the head of the file(s).

## Usage

Most of the scripts should contain a brief description on how to use them in the head of the main file or give some help text
if you run them without arguments (or with the '--help' argument).

## Content

 * **convert2mp3**:
    Two scripts (wma2mp3.pl and ogg2mp3.pl) to convert a batch of audio files to mp3 (including their tags).

 * **flash-copy**:
    Copy buffered Flash videos from your browsers temporary storage.

 * **mailwatcher-dbus**:
    A python script to check for new mails in one ore more IMAP accouts and publishes the result via DBus
    (can be used to display new mails in [awesome](http://awesome.naquadah.org) with the help of a textwidget,
    instructions in the head of mailwatcher.py).

 * **mailwatcher-xmobar**:
    A perl script to check multiple IMAP mailboxes for new mails and print a formated output, can be used with the xmobars Com plugin.
    With optional support for desktop notifications.

 * **misc**:
    A collection of random stuff.

 * **url-extract**:
    Extract all URLs from a given file and present them in a ncurses list.
    Open the URL selected in the list in a browser.

